﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TestController : ControllerBase
    {
        private const string Result = "Hello world";
        private readonly ILogger<TestController> _logger;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        public TestController(ILogger<TestController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        ///  异步接口
        /// </summary>
        /// <returns></returns>
        [HttpGet("AsyncGet")]
        public async Task<string> AsyncGet()
        {
            return await Task.Run(async() =>
            {
                await Task.Delay(3000);
                return Result;
            });
        }

        /// <summary>
        /// 同步接口
        /// </summary>
        /// <returns></returns>
        [HttpGet("SyncGet")]
        public string SyncGet()
        {
            return Task.Run(async () =>
            {
                await Task.Delay(3000);
                return Result;
            }).GetAwaiter().GetResult();
        }
    }
}
