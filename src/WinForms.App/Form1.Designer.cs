﻿
namespace WinForms.App
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_requestSyncApi = new System.Windows.Forms.Button();
            this.btn_requestAsyncApi = new System.Windows.Forms.Button();
            this.nud_requestCount = new System.Windows.Forms.NumericUpDown();
            this.nud_concurrentCount = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.rtb_printInfo = new System.Windows.Forms.RichTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btn_async = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.nud_deadTime = new System.Windows.Forms.NumericUpDown();
            this.btn_deadLock = new System.Windows.Forms.Button();
            this.btn_clear = new System.Windows.Forms.Button();
            this.btn_realDead = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.nud_requestCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_concurrentCount)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud_deadTime)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_requestSyncApi
            // 
            this.btn_requestSyncApi.Location = new System.Drawing.Point(625, 34);
            this.btn_requestSyncApi.Name = "btn_requestSyncApi";
            this.btn_requestSyncApi.Size = new System.Drawing.Size(115, 32);
            this.btn_requestSyncApi.TabIndex = 0;
            this.btn_requestSyncApi.Text = "请求同步接口";
            this.btn_requestSyncApi.UseVisualStyleBackColor = true;
            this.btn_requestSyncApi.Click += new System.EventHandler(this.btn_requestSyncApi_Click);
            // 
            // btn_requestAsyncApi
            // 
            this.btn_requestAsyncApi.Location = new System.Drawing.Point(459, 34);
            this.btn_requestAsyncApi.Name = "btn_requestAsyncApi";
            this.btn_requestAsyncApi.Size = new System.Drawing.Size(115, 32);
            this.btn_requestAsyncApi.TabIndex = 1;
            this.btn_requestAsyncApi.Text = "请求异步接口";
            this.btn_requestAsyncApi.UseVisualStyleBackColor = true;
            this.btn_requestAsyncApi.Click += new System.EventHandler(this.btn_requestAsyncApi_Click);
            // 
            // nud_requestCount
            // 
            this.nud_requestCount.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.nud_requestCount.Location = new System.Drawing.Point(83, 40);
            this.nud_requestCount.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nud_requestCount.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.nud_requestCount.Name = "nud_requestCount";
            this.nud_requestCount.Size = new System.Drawing.Size(120, 23);
            this.nud_requestCount.TabIndex = 2;
            this.nud_requestCount.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            // 
            // nud_concurrentCount
            // 
            this.nud_concurrentCount.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.nud_concurrentCount.Location = new System.Drawing.Point(298, 40);
            this.nud_concurrentCount.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nud_concurrentCount.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.nud_concurrentCount.Name = "nud_concurrentCount";
            this.nud_concurrentCount.Size = new System.Drawing.Size(120, 23);
            this.nud_concurrentCount.TabIndex = 3;
            this.nud_concurrentCount.Value = new decimal(new int[] {
            200,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "请求总次数:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(221, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "每批并发数:";
            // 
            // rtb_printInfo
            // 
            this.rtb_printInfo.Location = new System.Drawing.Point(12, 256);
            this.rtb_printInfo.Name = "rtb_printInfo";
            this.rtb_printInfo.Size = new System.Drawing.Size(888, 276);
            this.rtb_printInfo.TabIndex = 6;
            this.rtb_printInfo.Text = "";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn_requestSyncApi);
            this.groupBox1.Controls.Add(this.btn_requestAsyncApi);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.nud_requestCount);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.nud_concurrentCount);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(794, 87);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "同步异步接口测试";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btn_realDead);
            this.groupBox2.Controls.Add(this.btn_async);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.nud_deadTime);
            this.groupBox2.Controls.Add(this.btn_deadLock);
            this.groupBox2.Location = new System.Drawing.Point(12, 126);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(791, 100);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "winfow死锁测试";
            // 
            // btn_async
            // 
            this.btn_async.Location = new System.Drawing.Point(311, 39);
            this.btn_async.Name = "btn_async";
            this.btn_async.Size = new System.Drawing.Size(75, 23);
            this.btn_async.TabIndex = 8;
            this.btn_async.Text = "异步执行";
            this.btn_async.UseVisualStyleBackColor = true;
            this.btn_async.Click += new System.EventHandler(this.btn_async_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(1, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "死锁时间(秒):";
            // 
            // nud_deadTime
            // 
            this.nud_deadTime.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.nud_deadTime.Location = new System.Drawing.Point(83, 39);
            this.nud_deadTime.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.nud_deadTime.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.nud_deadTime.Name = "nud_deadTime";
            this.nud_deadTime.Size = new System.Drawing.Size(120, 23);
            this.nud_deadTime.TabIndex = 6;
            this.nud_deadTime.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // btn_deadLock
            // 
            this.btn_deadLock.Location = new System.Drawing.Point(221, 39);
            this.btn_deadLock.Name = "btn_deadLock";
            this.btn_deadLock.Size = new System.Drawing.Size(75, 23);
            this.btn_deadLock.TabIndex = 0;
            this.btn_deadLock.Text = "模拟死锁";
            this.btn_deadLock.UseVisualStyleBackColor = true;
            this.btn_deadLock.Click += new System.EventHandler(this.btn_imitateDeadLock_Click);
            // 
            // btn_clear
            // 
            this.btn_clear.Location = new System.Drawing.Point(12, 232);
            this.btn_clear.Name = "btn_clear";
            this.btn_clear.Size = new System.Drawing.Size(55, 23);
            this.btn_clear.TabIndex = 10;
            this.btn_clear.Text = "清屏";
            this.btn_clear.UseVisualStyleBackColor = true;
            this.btn_clear.Click += new System.EventHandler(this.btn_clear_Click);
            // 
            // btn_realDead
            // 
            this.btn_realDead.Location = new System.Drawing.Point(402, 39);
            this.btn_realDead.Name = "btn_realDead";
            this.btn_realDead.Size = new System.Drawing.Size(75, 23);
            this.btn_realDead.TabIndex = 9;
            this.btn_realDead.Text = "真实死锁";
            this.btn_realDead.UseVisualStyleBackColor = true;
            this.btn_realDead.Click += new System.EventHandler(this.btn_realDead_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(912, 544);
            this.Controls.Add(this.btn_clear);
            this.Controls.Add(this.rtb_printInfo);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "同步异步验证";
            ((System.ComponentModel.ISupportInitialize)(this.nud_requestCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_concurrentCount)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud_deadTime)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_requestSyncApi;
        private System.Windows.Forms.Button btn_requestAsyncApi;
        private System.Windows.Forms.NumericUpDown nud_requestCount;
        private System.Windows.Forms.NumericUpDown nud_concurrentCount;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox rtb_printInfo;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btn_deadLock;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown nud_deadTime;
        private System.Windows.Forms.Button btn_async;
        private System.Windows.Forms.Button btn_clear;
        private System.Windows.Forms.Button btn_realDead;
    }
}

