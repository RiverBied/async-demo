﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinForms.App
{
    public partial class Form1 : Form
    {

        private sealed class DeadTaskAsyncStateMachine : IAsyncStateMachine
        {
            public int state1;
            public AsyncTaskMethodBuilder<string> tBuilder;
            public Form1 form1;
            private string taskResult;
            private TaskAwaiter delay500Awaiter;
            private TaskAwaiter<string> helloWorldAwaiter;
            private void MoveNext()
            {
                int num = state1;
                string finalResult;
                try
                {
                    TaskAwaiter<string> awaiter;
                    TaskAwaiter awaiter2;
                    if (num != 0)
                    {
                        if (num == 1)
                        {
                            awaiter = helloWorldAwaiter;
                            helloWorldAwaiter = default(TaskAwaiter<string>);
                            num = (state1 = -1);
                            goto finalTag;
                        }
                        awaiter2 = Task.Delay(500).GetAwaiter();
                        if (!awaiter2.IsCompleted)
                        {
                            num = (state1 = 0);
                            delay500Awaiter = awaiter2;
                            DeadTaskAsyncStateMachine stateMachine = this;
                            tBuilder.AwaitUnsafeOnCompleted(ref awaiter2, ref stateMachine);
                            return;
                        }
                    }
                    else
                    {
                        awaiter2 = delay500Awaiter;
                        delay500Awaiter = default(TaskAwaiter);
                        num = (state1 = -1);
                    }
                    awaiter2.GetResult();
                    awaiter = Task.FromResult("Hello world").GetAwaiter();
                    // 因为awaiter.IsCompleted == true，下面代码进行移除
                    goto finalTag;
                finalTag:
                    finalResult = awaiter.GetResult();
                }
                catch (Exception exception)
                {
                    state1 = -2;
                    tBuilder.SetException(exception);
                    return;
                }
                state1 = -2;
                tBuilder.SetResult(finalResult);
            }

            void IAsyncStateMachine.MoveNext()
            {
                //ILSpy generated this explicit interface implementation from .override directive in MoveNext
                this.MoveNext();
            }

            private void SetStateMachine(IAsyncStateMachine stateMachine)
            {
            }

            void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
            {
                //ILSpy generated this explicit interface implementation from .override directive in SetStateMachine
                this.SetStateMachine(stateMachine);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private HttpClient _httpClient = new HttpClient();

        /// <summary>
        /// 
        /// </summary>
        private const string AsyncGetUrl = "http://localhost:5005/Test/AsyncGet";

        /// <summary>
        /// 
        /// </summary>
        private const string SyncGetUrl = "http://localhost:5005/Test/SyncGet";

        /// <summary>
        /// 
        /// </summary>
        public Form1()
        {
            InitializeComponent();
            CurrentSynchronizationContext = SynchronizationContext.Current;
            var controlToSendToField = typeof(WindowsFormsSynchronizationContext).GetField("controlToSendTo", BindingFlags.Instance | BindingFlags.NonPublic);
            controlToSendToField.SetValue(CurrentSynchronizationContext, this);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btn_requestAsyncApi_Click(object sender, EventArgs e)
        {
            await TryRequest("异步", AsyncGetUrl);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btn_requestSyncApi_Click(object sender, EventArgs e)
        {
            await TryRequest("同步", SyncGetUrl);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="url"></param>
        /// <returns></returns>
        private async Task TryRequest(string name, string url)
        {
            // 总请求次数
            var requestCount = nud_requestCount.Value;
            // 每批并发数
            var concurrentCount = nud_concurrentCount.Value;
            PrintInfo($"{name}接口调用开始执行: 总请求次数(requestCount),每批并发数({concurrentCount})");
            Stopwatch stopwatch = Stopwatch.StartNew();
            btn_requestAsyncApi.Enabled = false;
            btn_requestSyncApi.Enabled = false;
            // 
            if (requestCount < concurrentCount)
            {
                PrintInfo("总请求次数小于并发数，不执行。");
                return;
            }
            //
            for (decimal batch = 1; batch <= (requestCount / concurrentCount); batch++)
            {
                List<Task> tasks = new List<Task>();
                for (var taskIndex = 0; taskIndex <= concurrentCount; taskIndex++)
                {
                    tasks.Add(Task.Run(async () =>
                    {
                        try
                        {
                            var result = await _httpClient.GetAsync(url);
                        }
                        catch (Exception ex)
                        {
                            PrintInfo($"error:{ex.Message}");
                        }
                    }));
                }
                // 等待每批并发执行完成
                await Task.WhenAll(tasks);
            }
            btn_requestAsyncApi.Enabled = true;
            btn_requestSyncApi.Enabled = true;
            stopwatch.Stop();
            PrintInfo($"{name}接口调用执行完成:执行耗时({stopwatch.Elapsed})");
        }

        /// <summary>
        /// 
        /// </summary>
        private SynchronizationContext CurrentSynchronizationContext;

        /// <summary>
        /// 模拟死锁按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_imitateDeadLock_Click(object sender, EventArgs e)
        {
            var result = GetHelloWorldResult().Result;
            PrintInfo($"ManagedThreadId:{Thread.CurrentThread.ManagedThreadId} 打印结果:{result}");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private Task<string> GetHelloWorldResult()
        {
            TaskCompletionSource<string> _tcs = new TaskCompletionSource<string>();
            // 在主线程设置tcs的任务执行状态为完成
            Task.Run(() =>
            {
                CurrentSynchronizationContext.Post(c =>
                {
                    if (_tcs.Task.IsCompleted)
                    {
                        PrintInfo($"ManagedThreadId:{Thread.CurrentThread.ManagedThreadId} Ui线程设置任务完成状态失败.");
                    }
                    else
                    {
                        _tcs.SetResult($"ManagedThreadId:{Thread.CurrentThread.ManagedThreadId} Ui线程设置任务完成状态成功.");
                    }
                }, this);
            });
            // 超出阻塞时间终止Ui线程阻塞
            Task.Run(() =>
            {
                Thread.Sleep((int)nud_deadTime.Value * 1000);
                if (!_tcs.Task.IsCompleted)
                    _tcs.SetResult($"ManagedThreadId:{Thread.CurrentThread.ManagedThreadId} Hello World From 由于Ui线程阻塞，无法设置任务完成状态，由线程池线程设置任务完成状态.");
            });
            return _tcs.Task;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        protected override void WndProc(ref Message m)
        {
            if (m.Msg == GetThreadCallbackMessage())
            {
                var threadCallbackList = GetThreadCallbackList();
                PrintInfo($"触发WndProc:msg={m.Msg},threadCallbackList.Count={threadCallbackList.Count}");
                base.WndProc(ref m);
            }
            else
            {
                base.WndProc(ref m);
            }
        }

        private async void btn_async_Click(object sender, EventArgs e)
        {
            var result = await GetHelloWorldResult();
            PrintInfo($"ManagedThreadId:{Thread.CurrentThread.ManagedThreadId} 打印结果:{result}");
        }

        private void btn_clear_Click(object sender, EventArgs e)
        {
            rtb_printInfo.Clear();
        }

        private void btn_realDead_Click(object sender, EventArgs e)
        {
            string result = DeadTask().Result;
            PrintInfo(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private Task<string> DeadTask()
        {
            DeadTaskAsyncStateMachine stateMachine = new DeadTaskAsyncStateMachine();
            stateMachine.tBuilder = AsyncTaskMethodBuilder<string>.Create();
            stateMachine.form1 = this;
            stateMachine.state1 = -1;
            stateMachine.tBuilder.Start(ref stateMachine);
            return stateMachine.tBuilder.Task;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="printInfo"></param>
        private void PrintInfo(string printInfo)
        {
            rtb_printInfo.AppendText($"{printInfo}\r\n");
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        private (System.Collections.Queue, int) GetThreadCallback()
        {
            return (GetThreadCallbackList(), GetThreadCallbackMessage());
        }

        /// <summary>
        /// 获取需要在Ui线程执行的回调委托队列
        /// </summary>
        /// <returns></returns>
        private System.Collections.Queue GetThreadCallbackList()
        {
            var threadCallbackListFiled = typeof(Control).GetField("_threadCallbackList", BindingFlags.NonPublic | BindingFlags.Instance);
            return (System.Collections.Queue)threadCallbackListFiled.GetValue(this);
        }

        private static int _threadCallbackMessage = 0;

        /// <summary>
        /// 获取触发回调委托的窗口消息标识
        /// </summary>
        /// <returns></returns>
        private int GetThreadCallbackMessage()
        {
            if (_threadCallbackMessage == 0)
            {
                var threadCallbackMessageFiled = typeof(Control).GetField("s_threadCallbackMessage", BindingFlags.NonPublic | BindingFlags.Static);
                _threadCallbackMessage = Convert.ToInt32(threadCallbackMessageFiled.GetValue(null));
            }
            return _threadCallbackMessage;
        }

    }
}
